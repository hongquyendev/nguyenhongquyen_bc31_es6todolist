let dsHoatDong = [];
let completeActivities = [];
window.onload = () => {
  let content = layLocalStorage("Danh sach hoat dong");
  dsHoatDong = content;

  if (dsHoatDong) {
    renderHoatDong(dsHoatDong);
  }
  let complete = layLocalStorage("Danh sach hoat dong hoan thanh");
  completeActivities = complete;
  if (completeActivities) {
    renderCompleted(completeActivities);
  }
};
document.getElementById("addItem").onclick = () => {
  let hd = layHoatDong();
  dsHoatDong.push(hd);
  luuLocalStorage("Danh sach hoat dong", dsHoatDong);
  renderHoatDong(dsHoatDong);
};

let HoatDongModel = function (newTask) {
  this.newTask = newTask;
};

let layHoatDong = () => {
  let newTask = document.getElementById("newTask").value;

  let hoatDong = new HoatDongModel(newTask);

  return hoatDong;
};

let renderHoatDong = (arrHoatDong) => {
  let html = "";
  for (let index = 0; index < arrHoatDong.length; index++) {
    const hd = arrHoatDong[index];
    html += `
  <li>
      <h5>${hd.newTask}</h5>
      <div class="button">
          <i class="fa fa-trash-alt" onclick="xoaHoatDong(${index})"></i>
          <i class="fa fa-check-circle" onclick="checkButton(${index})"></i>
      </div>   
  </li>
  `;
  }
  return (document.getElementById("todo").innerHTML = html);
};

let luuLocalStorage = (name, arr) => {
  let mangHoatDongJSON = JSON.stringify(arr);
  localStorage.setItem(name, mangHoatDongJSON);
};

let layLocalStorage = (name) => {
  let output;
  if (localStorage.getItem(name)) {
    output = JSON.parse(localStorage.getItem(name));
  }
  return output;
};

// let timKiemViTri = (arr, value) => {
//   return arr.findIndex((item) => {
//     return item.index == value;
//   });
// };

let xoaHoatDong = (index) => {
  dsHoatDong.splice(index, 1);
  luuLocalStorage("Danh sach hoat dong", dsHoatDong);
  renderHoatDong(dsHoatDong);
};

let checkButton = (index) => {
  let hdComplete = dsHoatDong[index];
  completeActivities.push(hdComplete);
  xoaHoatDong(index);
  luuLocalStorage("Danh sach hoat dong hoan thanh", completeActivities);
  renderCompleted(completeActivities);
};

let renderCompleted = (arr) => {
  let html = "";
  for (let index = 0; index < arr.length; index++) {
    const hd = arr[index];
    html += `
    <li>
      <h5 class="complete">${hd.newTask}</h5>
      <div class="button">
          <i class="fa fa-trash-alt" onclick="xoaHoatDongHoanThanh(${index})"></i>
          <span class="fa fa-check-circle onclick="checkButton(${index})"></span>
      </div>   
    </li>
    `;
  }
  return (document.getElementById("completed").innerHTML = html);
};
let xoaHoatDongHoanThanh = (index) => {
  completeActivities.splice(index, 1);
  luuLocalStorage("Danh sach hoat dong hoan thanh", completeActivities);
  renderCompleted(completeActivities);
};
